import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  productName: string;
  productColor: string;
  otherParty: string;
  status: string;

  response: any;

  party = ['O=PartyB,L=New York,C=US',
    'O=PartyA,L=London,C=GB']

  color = ['Red', 'Black']

  empty = false;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.onGet();
  }

  onSubmit() {
    if (!this.productName || !this.productColor || !this.otherParty) {
      this.empty = true;
      return;
    }
    this.empty = false;
    const postBody = {
      productName: this.productName,
      productColor: this.productColor,
      otherParty: this.otherParty,
      status: 'Pending'
    }
    this.httpClient.post('http://localhost:10050/products', postBody,
      {
        headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json' })
      }
    ).subscribe(
      (dataList: any) => {
        alert(dataList.responseMessage)
        this.productColor='';
        this.productName='';
        this.otherParty='';
      },
      err => console.error,
      () => this.onGet()
    )
  }

  onGet() {
    this.httpClient.get('http://localhost:10050/getproduct').subscribe(
      dataList => {
        this.response = dataList;
        this.response.states=this.response.states.reverse();
      }
    )
  }


  updateStatus(id) {
    const body = {
      status: 'Received',
      linearId: id

    }
    this.httpClient.post('http://localhost:10050/receivedproducts', body).subscribe(
      (dataList: any) => {
        alert(dataList.responseMessage)
      },
      err => console.error(err),
      () => this.onGet()
    )
  }
}
